function [ xDataObj ] = calcHRMArb( gSpecIn, gSpecOut )
%CALCHRMARB As for calcHrzRegridMat, but allows for more generic LL grids

lonEdgeIn  = gSpecIn.lonEdge;
latEdgeIn  = gSpecIn.latEdge;
lonEdgeOut = gSpecOut.lonEdge;
latEdgeOut = gSpecOut.latEdge;
nXIn  = length(lonEdgeIn)  - 1;
nXOut = length(lonEdgeOut) - 1;
nYIn  = length(latEdgeIn)  - 1;
nYOut = length(latEdgeOut) - 1;

% Set up the output object
xDataObj.gridIn = [nXIn,nYIn];
xDataObj.gridOut = [nXOut,nYOut];
% Placeholder
xDataObj.xRegrid = [];

inSub = max(lonEdgeIn) - min(lonEdgeIn) < 359;
if inSub
    % Input grid is not global
    lonEdgeIn = [lonEdgeIn(end)-360, lonEdgeIn(:)'];
end
outSub = max(lonEdgeOut) - min(lonEdgeOut) < 359;
if outSub
    % Output grid is not global
    lonEdgeOut = [lonEdgeOut(end)-360, lonEdgeOut(:)'];
end

% Call the regridding function to do the actual work
[xLon,xLat] = calcLLXMat(lonEdgeIn,latEdgeIn,...
    lonEdgeOut,latEdgeOut);

% Remove the padding cell if the range was not global
if inSub
    xLon = xLon(:,2:end);
end
if outSub
    xLon = xLon(2:end,:);
end

% Start conversion to a generic regridding object
% This is equivalent to genRegridObj for tile file data
nElIn = nXIn*nYIn;
nElOut = nXOut*nYOut;

% Loop over all elements in the xLon/xLat matrices
[xLonI,xLonJ,xLonW] = find(xLon);
[xLatI,xLatJ,xLatW] = find(xLat);
nLonX = length(xLonI);
nLatX = length(xLatI);
fromVec = zeros(nLonX*nLatX,1);
toVec = zeros(nLonX*nLatX,1);
WVec = zeros(nLonX*nLatX,1);
iPt = 0;
for iLonPt = 1:nLonX
    % Current longitude match
    v1 = ones(size(xLatJ));
    lonToVec = v1.*xLonI(iLonPt);
    lonFromVec = v1.*xLonJ(iLonPt);
    
    % Assume cyclic
    OOB = lonToVec > nXOut;
    lonToVec(OOB) = lonToVec(OOB) - nXOut;
    OOB = lonFromVec > nXIn;
    lonFromVec(OOB) = lonFromVec(OOB) - nXIn;
    
    lonWeight = xLonW(iLonPt);
    ptRange = iPt + (1:nLatX);
    fromVec(ptRange) = sub2ind([nXIn,nYIn],lonFromVec,xLatJ);
    toVec(ptRange) = sub2ind([nXOut,nYOut],lonToVec,xLatI);
    WVec(ptRange) = lonWeight.*xLatW;
    iPt = iPt + nLatX;
end

% Output
xDataObj.xRegrid = sparse(fromVec,toVec,WVec,nElIn,nElOut);

end

